import React from "react";
import { connect } from "react-redux";
import { login, logout } from "../store/actions";
import { displayedInfos } from "../store/selectors";
import Login from "../components/Login";
import Profil from "../components/Profil";

const Home = ({
  login,
  logout,
  isAuthenticated,
  user,
  loginError,
  displayedInfos,
}) => {
  return (
    <>
      {isAuthenticated ? (
        <Profil logout={logout} user={user} infos={displayedInfos} />
      ) : (
        <Login submitLogin={login} loginError={loginError} />
      )}
    </>
  );
};
const mapStateToProps = (state) => ({
  user: state.user,
  displayedInfos: displayedInfos(state),
  loginError: state.loginError,
  isAuthenticated: state.isAuthenticated,
});

const mapDispatchToProps = { login, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Home);
