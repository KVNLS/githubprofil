import { createGlobalStyle } from "styled-components";
const theme = {
  primaryColor: "#0366d6",
  shadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
};

export const GlobalStyle = createGlobalStyle`
  body, html {
    height: 100%;
    margin: 0px;
    background: rgb(3, 102, 214);
    background: radial-gradient(
    circle,
    rgba(3, 102, 214, 1) 15%,
    rgba(0, 212, 255, 1) 100%
  );
  }
  #root {
    height: 100%;
  }
`;

export default theme;
