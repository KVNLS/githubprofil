export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGOUT = "LOGOUT";

export const GET_REPOS_SUCCESS = "GET_REPOS_SUCCESS";
export const GET_REPOS_ERROR = "GET_REPOS_ERROR";

export const GET_CONTRIBUTORS_SUCCESS = "GET_CONTRIBUTORS_SUCCESS";
export const GET_CONTRIBUTORS_ERROR = "GET_CONTRIBUTORS_ERROR";

export const GET_ORGS_SUCCESS = "GET_ORGS_SUCCESS";
export const GET_ORGS_ERROR = "GET_ORGS_ERROR";
