import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
  GET_REPOS_SUCCESS,
  GET_CONTRIBUTORS_SUCCESS,
  GET_ORGS_SUCCESS,
} from "./constants";

const initialState = {
  token: "",
  loginError: "",
  user: {},
  isAuthenticated: false,
  contributors: [],
  repos: [],
  orgs: [],
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payload.token,
        loginError: "",
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        token: "",
        loginError: action.payload.error,
      };
    case LOGOUT:
      return {
        ...state,
        user: {},
        isAuthenticated: false,
      };
    case GET_REPOS_SUCCESS:
      return {
        ...state,
        repos: action.payload.repos,
      };
    case GET_CONTRIBUTORS_SUCCESS:
      return {
        ...state,
        contributors: [
          ...state.contributors,
          { repo: action.payload.repo, value: action.payload.contributors },
        ],
      };
    case GET_ORGS_SUCCESS:
      return {
        ...state,
        orgs: action.payload.orgs,
      };
    default:
      return state;
  }
};

export default rootReducer;
