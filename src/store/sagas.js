import { takeEvery, put, call, select, all } from "redux-saga/effects";
import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  GET_REPOS_SUCCESS,
  GET_REPOS_ERROR,
  GET_CONTRIBUTORS_SUCCESS,
  GET_CONTRIBUTORS_ERROR,
  GET_ORGS_SUCCESS,
  GET_ORGS_ERROR,
} from "./constants";

const getFetchParams = (token) => ({
  method: "GET",
  headers: {
    Authorization: `token ${token}`,
  },
  mode: "cors",
  cache: "default",
});

function* fetchContributors({ payload }) {
  const { repo } = payload;
  try {
    const token = yield select((state) => state.token);
    const response = yield fetch(
      `https://api.github.com/repos/${repo.full_name}/stats/contributors`,
      getFetchParams(token)
    );
    const contributors = yield response.json();
    yield put({
      type: GET_CONTRIBUTORS_SUCCESS,
      payload: { contributors, repo: repo.id },
    });
  } catch (error) {
    yield put({
      type: GET_CONTRIBUTORS_ERROR,
      payload: { error: error.message },
    });
  }
}

function* fetchOrgs() {
  try {
    const token = yield select((state) => state.token);
    const response = yield fetch(
      `https://api.github.com/user/orgs`,
      getFetchParams(token)
    );
    const orgs = yield response.json();
    yield put({ type: GET_ORGS_SUCCESS, payload: { orgs } });
  } catch (error) {
    yield put({ type: GET_ORGS_ERROR, payload: { error: error.message } });
  }
}

function* fetchRepos({ payload }) {
  const { login } = payload;
  try {
    const token = yield select((state) => state.token);
    const response = yield fetch(
      `https://api.github.com/users/${login}/repos`,
      getFetchParams(token)
    );
    const repos = yield response.json();
    yield put({ type: GET_REPOS_SUCCESS, payload: { repos } });
    if (Array.isArray(repos) && repos.length > 0) {
      yield all(
        repos.map((repo) =>
          call(fetchContributors, { payload: { repo, login } })
        )
      );
    }
  } catch (error) {
    yield put({ type: GET_REPOS_ERROR, payload: { error: error.message } });
  }
}

function* authenticate({ payload }) {
  const { token } = payload;
  try {
    const response = yield fetch(
      "https://api.github.com/user",
      getFetchParams(token)
    );
    if (response.status === 200) {
      const user = yield response.json();
      yield put({ type: LOGIN_SUCCESS, payload: { user } });
      if (user?.login) {
        yield call(fetchRepos, { payload: { login: user.login } });
        yield call(fetchOrgs);
      }
    } else {
      throw new Error("Bad token");
    }
  } catch (error) {
    yield put({ type: LOGIN_ERROR, payload: { error: error.message } });
  }
  yield true;
}

export default function* rootSaga() {
  yield takeEvery("LOGIN", authenticate);
}
