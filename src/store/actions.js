import { LOGIN, LOGOUT } from "./constants";

export const login = ({ token }) => ({
  type: LOGIN,
  payload: {
    token,
  },
});

export const logout = () => ({
  type: LOGOUT,
});
