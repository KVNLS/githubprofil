import { createSelector } from "reselect";

const getUser = (state) => state.user;
const getRepos = (state) => state.repos;
const getContributors = (state) => state.contributors;
const getOrgs = (state) => state.orgs;

export const getNbCommits = createSelector(
  [getUser, getContributors],
  (user, contributors) => {
    const login = user?.login;
    return contributors.reduce((acc, repo) => {
      const contributions = repo?.value?.find(
        (contribution) => contribution?.author?.login === login
      );
      return acc + (contributions?.total || 0);
    }, 0);
  }
);

export const getTopRepos = createSelector([getRepos], (repos) =>
  repos
    .sort((repoA, repoB) => repoA?.stargazers_count > repoB?.stargazers_count)
    .slice(0, 3)
);

export const displayedInfos = createSelector(
  [getNbCommits, getTopRepos, getRepos, getOrgs],
  (nbCommits, topRepos, repos, orgs) => {
    //We parse orgs to have the same shape than repos
    const parsedOrgs = orgs?.map((org) => ({
      ...org,
      full_name: org?.login,
      html_url: `https://www.github.com/${org?.login}`,
    }));
    return { nbCommits, topRepos, orgs: parsedOrgs, nbRepos: repos.length };
  }
);
