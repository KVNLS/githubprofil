import styled from "styled-components";

export const StyledGithubCard = styled.div`
  height: 80px;
  overflow: hidden;
  background-color: white;
  padding: 20px;
  border-radius: 5px;
  box-shadow: ${(props) => props.theme.shadow};
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }
  h2 {
    margin: 10px 0px;
  }
  span {
    font-style: italic;
  }
`;
