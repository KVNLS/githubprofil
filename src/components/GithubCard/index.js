import React from "react";
import { StyledGithubCard } from "./styles";

const GithubCard = ({ title, link, desc }) => {
  return (
    <a href={link} target="_blank" rel="noreferrer noopener">
      <StyledGithubCard>
        <h2>{title}</h2>
        <span>{desc}</span>
      </StyledGithubCard>
    </a>
  );
};

export default GithubCard;
