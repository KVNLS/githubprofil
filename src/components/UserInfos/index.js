import React from "react";
import UserStats from "../UserStats";
import { StyledUserInfos } from "./styles";

const stats = [
  { label: "Number of commits", key: "nbCommits" },
  { label: "Number of repositories", key: "nbRepos" },
];

const UserInfos = ({ user, infos }) => {
  return (
    <StyledUserInfos>
      <img alt="avatar" src={user?.avatar_url} />
      <div>
        <h2>{user?.login}</h2>
        <div>
          <b>Bio:</b> <i>{user?.bio}</i>
        </div>
        <div>
          <b>Company:</b> {user?.company}
        </div>
        <div>
          <b>Blog:</b> {user?.blog}
        </div>
        <div>
          <b>Location:</b> {user?.location}
        </div>
        <div>
          {stats.map((stat) => (
            <UserStats key={stat.key} stats={stat} value={infos?.[stat.key]} />
          ))}
        </div>
      </div>
    </StyledUserInfos>
  );
};

export default UserInfos;
