import styled from "styled-components";

export const StyledUserInfos = styled.div`
  display: grid;
  border-radius: 0px 5px 5px 0px;
  grid-template-rows: 100px 1fr;
  grid-template-columns: 1fr;
  img {
    margin: 0px auto;
    position: relative;
    height: 200px;
    width: 200px;
    border-radius: 100px;
  }
  h2 {
    margin: 5px 0px;
  }

  > div {
    background: white;
    border-radius: 5px;
    padding: 100px 20px 20px 20px;
  }

  @media screen and (min-width: 1000px) {
    grid-template-rows: 200px;
    grid-template-columns: 100px 1fr;
    background-color: white;
    img {
      left: -100px;
      margin: 0px;
    }
    > div {
      padding: 20px;
    }
  }
`;
