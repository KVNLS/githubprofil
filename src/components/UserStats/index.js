import React from "react";
import { StyledUserStats } from "./styles";

const UserStats = ({ stats, value }) => {
  return (
    <StyledUserStats>
      <b>{stats.label}</b> : {value}
    </StyledUserStats>
  );
};

export default UserStats;
