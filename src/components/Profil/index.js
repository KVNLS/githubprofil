import React, { useMemo } from "react";
import { StyledProfil, StyledCardContainer } from "./styles";
import UserInfos from "../UserInfos";
import GithubCard from "../GithubCard";

const buildGithubCards = (data, label) => {
  return (
    <>
      <h2>{label}</h2>
      <StyledCardContainer>
        {data?.map((info) => (
          <GithubCard
            key={info.id}
            title={info.full_name}
            link={info.html_url}
            desc={info.description}
          />
        ))}
      </StyledCardContainer>
    </>
  );
};

const Profil = ({ user, logout, infos }) => {
  const repos = useMemo(
    () => buildGithubCards(infos.topRepos, "Repositories"),
    [infos.topRepos]
  );
  const orgs = useMemo(() => buildGithubCards(infos.orgs, "Organizations"), [
    infos.orgs,
  ]);
  return (
    <StyledProfil>
      <UserInfos user={user} infos={infos} />
      {repos}
      {orgs}
      <button onClick={logout}>Log out</button>
    </StyledProfil>
  );
};

export default Profil;
