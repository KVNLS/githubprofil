import styled from "styled-components";

export const StyledProfil = styled.div`
  max-width: 700px;
  margin: 0px auto;
  padding: 20px;
  button {
    float: right;
    margin: 20px;
  }
`;

export const StyledCardContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  grid-template-rows: auto;
  column-gap: 10px;
  row-gap: 15px;
  width: 100%;
  a {
    text-decoration: none;
    color: black;
  }
`;
