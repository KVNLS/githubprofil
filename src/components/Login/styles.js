import styled from "styled-components";

export const StyledLogin = styled.div`
  display: grid;
  height: 100vh;
`;

export const StyledCard = styled.div`
  height: 220px;
  width: 300px;
  margin: auto;
  border-radius: 3px;
  background: white;
  box-shadow: ${(props) => props.theme.shadow};

  h1 {
    text-align: center;
  }
  form {
    display: grid;
    height: 100%;
    grid-template-columns: 1fr;
    grid-template-rows: 80px 50px 1fr 40px;
  }

  input {
    margin: 10px;
    height: 30px;
  }
  button {
    background: #0077ff;
    border: none;
    color: white;
    border-radius: 0px 0px 3px 3px;
    font-size: 1.2em;
    cursor: pointer;
    &:hover {
      background: #0064d6;
    }
    &:active {
      background: #00499c;
    }
  }
`;

export const StyledError = styled.div`
  text-align: center;
  margin: auto;
`;
