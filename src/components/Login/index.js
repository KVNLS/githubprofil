import React from "react";
import { Formik, Form, Field, getIn } from "formik";
import { StyledLogin, StyledCard, StyledError } from "./styles";

const Login = ({ submitLogin, loginError }) => {
  return (
    <StyledLogin>
      <StyledCard>
        <Formik
          initialValues={{ token: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.token) {
              errors.token = "Required";
            } else if (!/^[0-9a-fA-F]{40}$/i.test(values.token)) {
              errors.token = "Invalid token";
            }
            return errors;
          }}
          onSubmit={(values, { resetForm }) => {
            submitLogin(values);
            resetForm();
          }}
        >
          {({ isSubmitting, touched, errors }) => (
            <Form>
              <h1>Login</h1>
              <Field type="token" name="token" placeholder="Github token" />
              <StyledError>
                {errors.token || (!getIn(touched, "token") && loginError)}
              </StyledError>
              <button type="submit" disabled={isSubmitting}>
                Submit
              </button>
            </Form>
          )}
        </Formik>
      </StyledCard>
    </StyledLogin>
  );
};

export default Login;
